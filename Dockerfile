FROM alpine

COPY Dockerfile /tmp

RUN apk add --no-cache --update \
    npm && \
    npm install -g dockerlint